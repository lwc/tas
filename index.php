<?php
require('core/autoloader/GenericLoader.php');

/*
 * inicjacja autoloadera i zarejestrowanie przestrzeni nazw
 */
$loader = new Opl\Autoloader\GenericLoader();
$loader -> addNamespace('core');
$loader -> addNamespace('model');
$loader -> addNamespace('controller');
$loader -> addNamespace('helper');
$loader -> register();

use core\Request;
use core\Router;
use core\Dispatcher;
use core\mysqlDatabase;

$request = new Request;

/*
 * inicjacja routera i ustawienie reguł dla routingu
 */
$router = new Router($request);
$router -> addRoute(array('name' => 'regex1', 'type' => 'regex', 'rule' => '%(?P<controller>\w+)/?(?P<action>\w+)?/?((?P<id>\d+))?$%'));

$router -> addRoute(array('name' => 'listOfProducts', 'rule' => 'product', 'controller' => 'Product', 'action' => 'index'));
$router -> addRoute(array('name' => 'listOfServices', 'rule' => 'service', 'controller' => 'Service', 'action' => 'index'));

$router -> addRoute(array('name' => 'default', 'rule' => '', 'controller' => 'Home', 'action' => 'index'));

$router -> addRoute(array('name' => 'loginPage', 'rule' => 'login', 'controller' => 'Login', 'action' => 'index'));
$router -> addRoute(array('name' => 'logout', 'rule' => 'logout', 'controller' => 'Logout', 'action' => 'index'));

$router -> addRoute(array('name' => 'Product', 'rule' => 'offer/add', 'controller' => 'Product', 'action' => 'addProduct'));

$router -> addRoute(array('name' => 'addOffer', 'rule' => 'one_service', 'controller' => 'Service', 'action' => 'get'));

$router -> addRoute(array('name' => 'Service', 'rule' => 'one_service', 'controller' => 'Service', 'action' => 'get'));

$router -> addRoute(array('name' => 'Sub', 'rule' => 'subs/add', 'controller' => 'Subs', 'action' => 'add'));

$router -> addRoute(array('name' => 'aOffer', 'rule' => 'admin/', 'controller' => 'Admin', 'action' => 'getOffers'));
$router -> addRoute(array('name' => 'aCategories', 'rule' => 'admin/categories', 'controller' => 'Admin', 'action' => 'getCategories'));

$router -> addRoute(array('name' => 'aProducts', 'rule' => 'admin/products', 'controller' => 'Admin', 'action' => 'getProducts'));

$router -> route();

/*
 * inicjacja dispatchera i przekazanie mu obiektu routera
 */
$dispatcher = new Dispatcher($router);
$dispatcher -> run();

