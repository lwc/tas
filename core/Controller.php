<?php

namespace core;

use core\Config;

abstract class Controller {
     
    public function __construct() {}
    
    abstract public function index();
    
    public static function base() {
        return Config::load('core', 'baseUrl');
    }
    
    public static function site() {
        return Config::load('core', 'siteUrl');
    }
    
    public function redirect($location) {
        header("location: ".$location."");
    }
}