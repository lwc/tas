<?php

namespace core;
use core\Config;
use core\request\iRequest;

class Request implements iRequest {
    
    private $post;
    private $get;
    private $session;
    private $url;
    private $vars = array();

    public function __construct() {
        $this -> post = (isset($_POST)) ? $_POST : array();
        $this -> get = (isset($_GET)) ? $_GET : array();
        $this -> session = (isset($_SESSION)) ? $_SESSION : array();
        $this -> url = $_SERVER['REQUEST_URI'];  
    }
    
    /*
     * zwraca zmienną get po kluczu
     */
    public function get($key) {
        if(isset($this -> get[$key]))
            return $this -> get[$key];
    }
    
     /*
     * zwraca zmienną post po kluczu
     */
    public function post($key) {
        if(isset($this -> post[$key]))
            return $this -> post[$key];
    }
    
     /*
     * zwraca zmienną sesyjną po kluczu
     */
    public function session($key) {
        if(isset($this -> session[$key]))
            return $this -> session[$key];
    }
    
    /*
     * zwraca zmienną dowolnego typu po nazwie
     */
    public function getVar($name) {
        $this -> vars[] = $this -> get($name); 
        $this -> vars[] = $this -> post($name);
        $this -> vars[] = $this -> session($name);
        
        foreach($this -> vars as $key => $value)
            if(isset($this -> vars[$key]))
                return $this -> vars[$key];
        
    }
    
    /*
     * zwraca url bez ścieżki do skryptu oraz domeny
     */
    public function getUrl() {
        return str_replace(Config::load('request', 'path'), '', $this -> url);
    }
}

