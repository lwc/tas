<?php

namespace core;

class View {
	
    public function __construct($moduleView) {
		$this -> moduleView = $moduleView;
                $this -> siteUrl = Config::load('core', 'siteUrl');
    }
    
    public function render($type="home") { 
        if($type == "home")
            require_once 'templates/index.php';
        elseif($type == "admin")
            require_once 'templates/admin.php';
    }
}