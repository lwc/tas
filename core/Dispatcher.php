<?php

namespace core;

use core\View;
use core\Config;

class Dispatcher {
    
    /*
     * wymusza przekazanie obiektu Router
     * pobiera z Routera dane dotyczące
     * kontrolera, akcji i parametrów z url
     */
    public function __construct(Router $router) {
        $this -> router = $router;
        $this -> data = $this -> router -> getRoute();
    }
    
    /*
     * tworzy kontroler i odpala metodę 
     * przekazuje wynik do widoku
     */
    public function run() {
        
      if(!file_exists("controller/".$this -> data['controller'] . '.php')) {
        $className = 'controller\\' . Config::load('core', 'defaultController');   
        $methodName = Config::load('core', 'defaultAction');
      }
      else {
        $className = 'controller\\' . $this -> data['controller'];
        $methodName = $this -> data['action'];
      }
      
      $params = $this -> data['params'];
     
      $reflectObject = new \ReflectionClass($className);
      $object = $reflectObject -> newInstance();
	  
      if($reflectObject -> hasMethod($methodName)) {
        $reflectMethod = new \ReflectionMethod($className, $methodName);
		$object -> $methodName($params);
        /*if($reflectMethod -> getNumberOfParameters() > 0) {
            $view = new View($object -> $methodName($params));
            $view -> render();
        }
        else {
            $view = new View($methodName);
            $view -> render();
        }*/ 
      }
      
    }
    
}
