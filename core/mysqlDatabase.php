<?php

namespace core;

use core\database\iDatabase;
use core\config;

class mysqlDatabase implements iDatabase {
  
  private static $_instance = null;
  
  private function __construct() {}

  public static function getInstance() {
    if(!(self::$_instance instanceof mysqlDatabase))
      self::$_instance = new \PDO(
              'mysql:host='.Config::load('database', 'host').';dbname='.Config::load('database', 'dbname').';charset=utf8', 
              Config::load('database', 'username'),
              Config::load('database', 'password')
              );
    
    return self::$_instance;
  }
}
?>