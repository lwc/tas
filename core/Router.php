<?php

namespace core;

use core\Core;
use core\request\iRequest;
use core\Config;

class Router {
     
    private $controller;
    private $action;
    private $request;
    private $url;
    private $regex_routes = array();
    private $static_routes = array();
    
    public function __construct(iRequest $request) {
        $this -> request = $request;
        $this -> url = $this -> request -> getUrl();
    }
    
    public function route() {
        $defaults = array('controller' => Config::load('core', 'defaultController'), 'action' => Config::load('core', 'defaultAction'), 'id' => '');
        
        foreach($this -> static_routes as $route) {         
                if($this -> url == $route['rule']) {
                    $this -> controller = $route['controller'];
                    $this -> action = $route['action'];
                }      
        }
        
        if(empty($this -> controller) && empty($this -> action)) {
            foreach($this -> regex_routes as $route) { 
                preg_match_all('/P\<(\w+)\>/', $route['rule'], $matches, PREG_PATTERN_ORDER);
                $params = $matches[1];
                
                preg_match($route['rule'], $this -> url, $matches);
                
                $vars = array();
                foreach($params as $param)
                    $vars[$param] = (isset($matches[$param]) && !empty($matches[$param])) ? $matches[$param] : $defaults[$param]; 
                
            }
            
            if(isset($vars['controller']) && !isset($vars['action'])) {
                
                    $this -> controller = $vars['controller'];
                    $this -> action = $defaults['action'];
                    
            } elseif(isset($vars['controller']) && isset($vars['action'])) {  
                
                    $this -> controller = $vars['controller'];
                    $this -> action = $vars['action'];
                    
                    array_shift($vars);
                    array_shift($vars);
                    
                    $this -> params = $vars;
            } else {
                    $this -> controller = $defaults['controller'];
                    $this -> action = $defaults['action'];
            }    
            
        }
              
    }
    
    public function getRoute() {
        return array('controller' => ucfirst($this -> controller), 'action' => $this -> action, 'params' => $this -> params);
    }
    
    public function addRoute(array $route) {
        if(isset($route['name']) && isset($route['rule']))
            if(isset($route['type']) && $route['type'] == 'regex')
                $this -> regex_routes[] = $route;
            else
                $this -> static_routes[] = $route;
    }
}