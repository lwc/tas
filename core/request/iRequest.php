<?php

namespace core\request;

interface iRequest {
    
    public function getVar($name);
}