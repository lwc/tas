<?php

namespace core;
use core\config\xmlConfig;

class Config {
    
    private $type;

    /*
     * ładuję żądany wpis konfiguracyjny z danej kolekcji
     * w zależności od typu, czyli gdzie są przetrzymywane dane
     * domyślnie xml
     */
    public static function load($collection, $name, $type = 'xml') {
  	switch ($type) {
		case 'xml':
                    $xml = new xmlConfig();
                    return $xml -> load($collection, $name);
                break;
        }
    }
        
}