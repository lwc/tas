<?php

namespace core\config;

interface iConfig {
    
    public function load($collection, $name);
}