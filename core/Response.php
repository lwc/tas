<?php

namespace core;

class Response {
    
    private $file = 'config/response.xml';
    
    public function __construct() {
        $dom = new \DOMDocument();
        $dom -> load($this -> file);
        $this -> xpath = new \DOMXPath($dom);  
    }
    
    public function load($collection, $name) {
        $names = $this -> xpath -> query('/response/'.$collection.'//'.$name.'');

        foreach($names as $name)
           return (string)$name -> nodeValue;
    }
}