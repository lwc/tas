<?php

namespace controller;

use core\Controller;
use core\Request;
use core\View;

session_start();

class Logout extends Controller {
    
    public function __construct() {
        parent::__construct();
        $this -> request = new Request;
    }
    
    public function index() {
		session_destroy();
		header("location: home");
    }
        
}
    
    