<?php

namespace controller;

use core\Controller;
use core\Request;
use core\View;
use core\Response;

class Product extends Controller {
    
    public function __construct() {
        parent::__construct();
        $this -> request = new Request;
        $this -> response = new Response;
    }
    
    public function index() {
        $view = new View('product'); 
        
        $url="http://s384023.iis.wmi.amu.edu.pl/api/categories";
        $json = file_get_contents($url);
        $data = json_decode($json, TRUE);
        
        $url="http://s384023.iis.wmi.amu.edu.pl/api/products";
        $json = file_get_contents($url);
        $data2 = json_decode($json, TRUE);
        
        $view -> data = $data;
        $view -> data2 = $data2;
	$view -> render();
    }
    
    public function get($data) {
        $view = new View('one_product'); 
        
        $url = "http://s384023.iis.wmi.amu.edu.pl/api/products/".$data['id']."";
        $json = file_get_contents($url);
        $data = json_decode($json, TRUE);
        
        $view -> data = $data;
	$view -> render();
    }
    
    public function addProduct() {
        $view = new View('offer');
        
        $data = array(
            "Name" => $this -> request -> post("name"), 
            "ProductInformation" => $this -> request -> post("description"),
            "CategoryId" => $this -> request -> post("category")
                );                                                                    
        $data_string = json_encode($data);                                                                            

        $ch = curl_init('http://s384023.iis.wmi.amu.edu.pl/api/products');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   

        $result = curl_exec($ch);

        $view -> response = $this -> response -> load('offer', 'info');
        
	$view -> render();
    }
        
}
    
    