<?php

namespace controller;

use core\Controller;
use core\Request;
use core\View;

class Offer extends Controller {
    
    public function __construct() {
        parent::__construct();
        $this -> request = new Request;
    }
    
    public function index() {
	$view = new View('offer'); 
	$view -> render();
    }       
}
    
    