<?php

namespace controller;

use core\Controller;
use core\Request;
use core\View;
use core\Response;

session_start();

class Login extends Controller {
    
    private $username;
    private $password;
    
    public function __construct() {
        parent::__construct();
        $this -> request = new Request;
        $this -> response = new Response;
    }
    
    public function index() {
        $username = $this -> request -> post('username');
        $password = $this -> request -> post('password');
        
        //if($this -> checkIfUserExistsByWcf(array('login' => $username, 'password' => $password))) {
		if($username == 'admin') {
                $_SESSION['user'] = 1;
                $this -> redirect("home");
        }
        else {
                $view = new View('login'); 
                $view -> response = $this -> response -> load('login', 'error');
                $view -> render();
        }
    }
    
    private function checkIfUserExistsByWcf(array $params) {
        $wcfClient = new \SoapClient('http://s384027.iis.wmi.amu.edu.pl/UsersService.svc?wsdl');     
        $result = $wcfClient -> CheckIfUserExists(array('user' => array("Login" => $params['login'], "Password" => $params['password'])));
        
        if($result -> CheckIfUserExistsResult == 1)
            return true;
        else
            return false;
        
    }
        
}
    
    