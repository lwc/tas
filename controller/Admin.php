<?php

namespace controller;

use core\Controller;
use core\Request;
use core\View;

class Admin extends Controller {
    
    public function __construct() {
        parent::__construct();
        $this -> request = new Request;
    }
    
    public function index() {
        $view = new View('admin');     
	$view -> render("admin");
    }
    
    public function getOffers() {
        $view = new View('admin_offer');
	$view -> render("admin");
    }
    
    public function getCategories() {
        $view = new View('admin_categories');
        
        $url = "http://s384023.iis.wmi.amu.edu.pl/api/Categories/";
        $json = file_get_contents($url);
        $data = json_decode($json, TRUE);
        
        $view -> data = $data;
	$view -> render("admin");
    }
    
    public function deleteCategory($data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://s384023.iis.wmi.amu.edu.pl/api/Categories/".$data['id']."");
        
        curl_setopt($ch, CURLOPT_ENCODING ,"utf-8");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        
        $output = curl_exec($ch);

        curl_close($ch);
        
        $this -> redirect("http://localhost/skrypty/ceneo/admin/categories");
    }  
    
    public function addCategory() {
        $data = array("Name" => $this -> request -> post("name"));                                                                    
        $data_string = json_encode($data);                                                                            

        $ch = curl_init('http://s384023.iis.wmi.amu.edu.pl/api/Categories');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   

        $result = curl_exec($ch);
        
        $this -> redirect("http://localhost/skrypty/ceneo/admin/categories");
    } 
    
    public function updateCategory($data) {
        $view = new View('admin_update_category');
        
        $id = $data['id'];
        
        $url = "http://s384023.iis.wmi.amu.edu.pl/api/Categories/".$id."";
        $json = file_get_contents($url);
        $data = json_decode($json, TRUE);
        
        $view -> name = $data['Name'];

        if(isset($_POST['submit'])) {
            echo $this -> request -> post("name");
            $data = array("Name" => $this -> request -> post("name"));                                                                    
            $data_string = json_encode($data);
            
            echo 'http://s384023.iis.wmi.amu.edu.pl/api/Categories/'.$data['id'].'';
            $ch = curl_init('http://s384023.iis.wmi.amu.edu.pl/api/Categories/'.$id.'');                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                       
            );                                                                                                                   

            $result = curl_exec($ch);
            
            $this -> redirect("http://localhost/skrypty/ceneo/admin/updateCategory/".$id."");
        }
        
        $view -> render("admin");
    }
    
    //Produkty
    
    public function getProducts() {
        $view = new View('admin_products');
        
        $url = "http://s384023.iis.wmi.amu.edu.pl/api/products/";
        $json = file_get_contents($url);
        $data = json_decode($json, TRUE);
        
        $view -> data = $data;
	$view -> render("admin");
    }
    
    public function deleteProduct($data) {
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://s384023.iis.wmi.amu.edu.pl/api/products/".$data['id']."");
        
        curl_setopt($ch, CURLOPT_ENCODING ,"utf-8");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        
        $output = curl_exec($ch);
        
        curl_close($ch);
        
        $this -> redirect("http://localhost/skrypty/ceneo/admin/products");
    } 
    
    public function updateProduct($data) {
        $view = new View('admin_update_product');
        
        $id = $data['id'];
        
        $url = "http://s384023.iis.wmi.amu.edu.pl/api/products/".$id."";
        $json = file_get_contents($url);
        $data = json_decode($json, TRUE);

        $view -> name = $data['Name'];
        $view -> description = $data['ProductInformation'];
        $view -> flag = $data['Flag'];
        
        if(isset($_POST['submit'])) {
            $data = array(
                "Name" => $this -> request -> post("name"),
                "ProductInformation" => $this -> request -> post("description"),
                "Flag" => $this -> request -> post("flag")); 
            $data_string = json_encode($data);
            
            $ch = curl_init('http://s384023.iis.wmi.amu.edu.pl/api/products/'.$id.'');                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                       
            );                                                                                                                   

            $result = curl_exec($ch);
            
            $this -> redirect("http://localhost/skrypty/ceneo/admin/updateProduct/".$id."");
        }
        
        $view -> render("admin");
    }
}
    
    