<?php

namespace controller;

use core\Controller;
use core\Request;
use core\View;
use core\Response;

class Subs extends Controller {
    
    public function __construct() {
        parent::__construct();
        $this -> request = new Request;
        $this -> response = new Response;
    }
    
    public function index() {
	$view = new View('subs'); 
	$view -> render();
    }
    
    public function add() {
        $view = new View('subs'); 
        
        $category = $this -> request -> post('category');
        
        $wcfClient = new \SoapClient('http://s384027.iis.wmi.amu.edu.pl/SubscriptionsService.svc?wsdl');
        $result = $wcfClient -> AddSubscription(array('userId' => 1, 'categoryId' => $category));
	
        if($result -> AddSubscriptionResult == 1)
            $view -> response = $this -> response -> load('subs', 'info');
        
	$view -> render();
        
    }
        
}
    
    