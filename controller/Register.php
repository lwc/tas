<?php

namespace controller;

use core\Controller;
use core\Request;
use core\View;

class Register extends Controller {
    
    public function __construct() {
        parent::__construct();
        $this -> request = new Request;
    }
    
    public function index() {
        $view = new View('register'); 
		$view -> render();
    }
        
}
    
    