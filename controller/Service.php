<?php

namespace controller;

use core\Controller;
use core\Request;
use core\View;

class Service extends Controller {
    
    public function __construct() {
        parent::__construct();
        $this -> request = new Request;
    }
    
    public function index() {
        $view = new View('service'); 
		$view -> render();
    }
    
    public function get($data) {
        $view = new View('one_service'); 
        $view -> id = $data['id'];
	$view -> render();
    }
        
}
    
    