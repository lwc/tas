<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">

    <title>Panel administracyjny</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../dist/css/dashboard.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../dist/js/bootstrap.min.js"></script>
    <script src="../assets/js/docs.min.js"></script>
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="">Panel zarządzania systemem Ceneo</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../">Powrót</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="">Strona główna</a></li>
            <li><a href="">Propozycje</a></li>
            <li><a href="#">Subskrypcja</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

          <h2 class="sub-header">Propozycje produktów/usług</h2>
          <div class="table-responsive" style="max-width: 800px;">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nazwa</th>
                  <th>Użytkownik</th>
                  <th>Akcja</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Karcher SC 1.020 (1.512-211.0)</td>
                  <td>testowy</td>
                  <td>
				   <button type="button" class="btn btn-xs btn-primary">dodaj</button>
				   <button type="button" class="btn btn-xs btn-danger">usuń</button>
				  </td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Casio FX-991ES PLUS</td>
                  <td>hortex92</td>
                  <td>
				   <button type="button" class="btn btn-xs btn-primary">dodaj</button>
				   <button type="button" class="btn btn-xs btn-danger">usuń</button>
				  </td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Optoma Hd131Xe </td>
                  <td>ewelina93</td>
                  <td>
				   <button type="button" class="btn btn-xs btn-primary">dodaj</button>
				   <button type="button" class="btn btn-xs btn-danger">usuń</button>
				  </td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>Motorola czytnik kodów kreskowych (LS2208)</td>
                  <td>pikachu</td>
                  <td>
				   <button type="button" class="btn btn-xs btn-primary">dodaj</button>
				   <button type="button" class="btn btn-xs btn-danger">usuń</button>
				  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
	
  </body>
</html>
