    <div class="content">

      <form class="form-signin" role="form">
        <h2 class="form-signin-heading">Rejestracja</h2>
        <input type="email" class="form-control" placeholder="adres e-mail" required autofocus>
        <input type="password" class="form-control" placeholder="hasło" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Zarejestruj</button>
      </form>

    </div>