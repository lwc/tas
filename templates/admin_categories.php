<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    
 <h2 class="sub-header">Kategorie</h2>
  
 <h3 class="sub-header" style="margin-left: 90px;">Dodaj kategorię</h3>
	
 <form class="form-horizontal" role="form" method="post" action="<?php echo $this -> siteUrl;?>admin/addCategory">
  <div class="form-group">
    <label class="col-sm-2 control-label">Nazwa</label>
    <div class="col-sm-10" style="width: 300px;">
      <input name="name" type="text" class="form-control" placeholder="nazwa" required>
    </div>
  </div>
    
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">dodaj</button>
    </div>
  </div>
  
 </form>
  
  <div class="table-responsive" style="max-width: 800px;">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Nazwa</th>
          <th>Akcja</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $n = count($this -> data);
        
        for($i=0;$i<$n;$i++) {
        ?>        
        <tr>
          <td><?php echo $this -> data[$i]['Id']; ?></td>
          <td><?php echo $this -> data[$i]['Name']; ?></td>
          <td>
            <a href="<?php echo $this -> siteUrl;?>admin/updateCategory/<?php echo $this -> data[$i]['Id']; ?>"><button type="button" class="btn btn-xs btn-primary">edytuj</button></a>
            <a href="<?php echo $this -> siteUrl;?>admin/deleteCategory/<?php echo $this -> data[$i]['Id']; ?>"><button type="button" class="btn btn-xs btn-danger">usuń</button></a>
          </td>
        </tr>
        <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>