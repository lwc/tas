<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">

    <title>Panel administracyjny</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $this -> siteUrl; ?>dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $this -> siteUrl; ?>dist/css/dashboard.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="<?php echo $this -> siteUrl; ?>ist/js/bootstrap.min.js"></script>
    <script src="<?php echo $this -> siteUrl; ?>assets/js/docs.min.js"></script>
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo $this -> siteUrl;?>admin">Panel zarządzania systemem Ceneo</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../">Powrót</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="<?php echo $this -> siteUrl;?>admin">Strona główna</a></li>
            <li><a href="<?php echo $this -> siteUrl;?>admin">Propozycje</a></li>
            <li><a href="<?php echo $this -> siteUrl;?>admin/categories">Kategorie</a></li>
            <li><a href="<?php echo $this -> siteUrl;?>admin/products">Produkty</a></li>
          </ul>
        </div>

      </div>
    </div>
      
      	  <?php
	   require_once 'templates/'.$this -> moduleView.'.php';
	  ?>

  </body>
</html>
