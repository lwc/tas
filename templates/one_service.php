<div class="content">
    
    <div class="product" style="margin: 20px;">
        <p style="font-size: 25px; font-weight: bold; font-family: Tahoma;">Pogotowie Komputerowe 24 Poznań</p>
        <img src="http://img26.olx.pl/images_tablicapl/86826371_1_644x461_pogotowie-komputerowe-24-7-z-dojazdem-poznan.jpg">
        <p style="font-size: 16px; font-family: Tahoma; text-align: justify;">
           Oferuję Państwu szeroko pojętą pomoc komputerową z dojazdem do klienta na terenie Poznania i okolic
           przez 24 godz. na dobę, 7 dni w tygodniu, 365 dni w roku!!!<br>
Rozwiążę wszelkie Państwa problemy związane z komputerem, usunę wirusy i trojany, zainstaluję system Windows i wszelkie inne oprogramowanie, odzyskam utracone dane, pomogę w modernizacji komputera. Stworzę również dla Państwa stronę internetową oraz pomogę w wyborze domeny i hostingu. Każdy problem z komputerem to dla mnie realizacja mojej pasji, którą są komputery a każdy naprawiony sprzęt jest dla mnie utwierdzeniem w tym co robię.  
<br>Ceny moich usług są bardzo konkurencyjne i przystępne. Za niesatysfakcjonującą usługę nie pobieram żadnych opłat. Każdy problem traktuję indywidulalnie i rozwiązuje go z prawdziwą troską o klienta i jego portfel! 
        </p>
        <p style="font-size: 15px; text-decoration: underline;">Ocena: 4.86 (16 opinii)</p>
        <br>
        <p style="font-size: 20px; font-family: Tahoma;">Opinie</p>
        
        <script>
        $(document).ready(function(){
          $("button").click(function(){
            $("form").toggle();
          });
        });
        </script>
        
        <div>
            <button>Dodaj własną opinię</button>
 <form class="form-horizontal" role="form" method="post">
  <div class="form-group">
    <label class="col-sm-2 control-label">Ocena</label>
    <div class="col-sm-10" style="width: 300px;">
      <input name="title" type="text" class="form-control" placeholder="ocena" required>
    </div>
  </div>
    
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Treść</label>
    <div class="col-sm-10" style="width: 500px;">
      <textarea id="txt_content2" class="form-control" rows="3" name="short_content" required>opis...</textarea>
    </div>
  </div>
    
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default" name="submit">Wyślij</button>
    </div>
  </div>
  
 </form>
        </div>
        <br>
        <div class="opinion">
            #1 (Ocena: 4.0)<br><br>
            <p>Naprawde pomocny zwłaszcza w sprzątaniu łazienki, długo zastanawialam się nad zakupem tego parowaru jednak teraz nie żałuję zakupu i na prawdę polecam wszystkim a zwlaszcza tym którzy mają w domu dużą powierzchnie wyłożoną glazurą. </p>
            <p>Autor: tomek93</p>
            
            #2 (Ocena: 5.0)<br><br>
            <p>Karcher testowałam na różnych powierzchniach tj. kafelki, panele. Działa super - no oprócz paneli. Na panelach pozostawia widoczne po wyschnięciu smugi, które niestety należy przetrzeć na nowo wilgotną szmatką. Może jest to wina cienkiej szmatki do parownicy ? Ogólnie samo mycie powierzchni ok. 98m2 zajmuje niecałe 10 minut. POLECAM :)</p>
            <p>Autor: adam342525</p>
        </div>
    </div>

</div>