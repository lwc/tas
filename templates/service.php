	<div class="content">

        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation" style="margin-top: 10px">
          <div class="list-group">
            <a href="#" class="list-group-item active">Kategorie</a>
            <a href="#" class="list-group-item">Informatyka</a>
            <a href="#" class="list-group-item">Motoryzacja</a>
            <a href="#" class="list-group-item">Medycyna</a>
          </div>
        </div>
		
          <h2 class="sub-header">Usługi</h2>
          <div class="table-responsive" style="max-width: 800px;">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nazwa</th>
                  <th>Ilość opinii</th>
                  <th>Ocena</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Zakład Mechaniczny Szczublewski Wojciech</td>
                  <td>91</td>
                  <td>4.21</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Dr n. med. Wojciech Chiciak Chirurgia plastyczna</td>
                  <td>54</td>
                  <td>4.74</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td><a href="<?php echo $this -> siteUrl; ?>/service/get/1">Pogotowie Komputerowe 24 Poznań</a></td>
                  <td>19</td>
                  <td>4.86</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
	
</div>