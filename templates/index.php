<?php 
session_start(); 
?>
<html>
  <head>
    <meta charset="utf-8">

    <title>Ceneo</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $this -> siteUrl; ?>dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $this -> siteUrl; ?>dist/css/main.css" rel="stylesheet">
    <link rel=icon href="<?php echo $this -> siteUrl; ?>dist/favicon.ico" type="image/ico">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <script src="<?php echo $this -> siteUrl; ?>dist/js/ie-emulation-modes-warning.js"></script>

    
  </head>

  <body>

    <div class="navbar navbar-default navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="Home">Ceneo</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo $this -> siteUrl; ?>Home">Strona główna</a></li>
            <li><a href="<?php echo $this -> siteUrl; ?>product">Produkty</a></li>
            <li><a href="<?php echo $this -> siteUrl; ?>service">Usługi</a></li>
			<?php
			if($_SESSION['user'] == 1) {
			?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Akcje<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo $this -> siteUrl; ?>offer">Zaproponuj produkt/usługę</a></li>
                <li><a href="<?php echo $this -> siteUrl; ?>subs">Subskrypcja</a></li>
				<li><a href="http://localhost/skrypty/ceneo/admin">Panel administracyjny</a></li>
              </ul>
            </li>
			<?php
			}
			?>
          </ul>
		  <?php
		  if($_SESSION['user'] !== 1) {
		  ?>
		  <form class="navbar-form navbar-right" role="form" action="login" method="post">
            <div class="form-group">
              <input name="username" type="text" placeholder="login" class="form-control">
            </div>
            <div class="form-group">
              <input name="password" type="password" placeholder="hasło" class="form-control">
            </div>
            <button type="submit" class="btn btn-success" name="submit">Zaloguj</button>
			<a href="<?php echo $this -> siteUrl; ?>register"><button type="button" class="btn btn-primary">Zarejestruj</button></a>
          </form>
		  <?php
		  } else {
		  ?>
		    <ul class="nav navbar-nav navbar-right">
              <li><a href="<?php echo $this -> siteUrl; ?>logout">Wyloguj</a></li>
            </ul>
		  <?php
		  }
		  ?>
        </div>
      </div>
    </div>
	
    <div class="container">

	<div class="header">
	 Ceneo - Serwis umożliwiający ocenę produktów i usług. Dla zarejestrowanych użytkowników dajemy możliwość komentowania oraz subskrybowania naszej aktualnej oferty!
	</div>
	
      <div class="jumbotron">
			<form class="navbar-form">
              <input size="130" type="text" placeholder="Nazwa produktu..." class="form-control">
			  <button type="submit" class="btn btn-success">Szukaj</button>
			  <a href="">Wyszukiwanie zaawansowane</a>
			</form>
      </div>
	  
	  <?php
	   require_once 'templates/'.$this -> moduleView.'.php';
	  ?>
	  
    </div>

    <script src="<?php echo $this -> siteUrl; ?>dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo $this -> siteUrl; ?>dist/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
