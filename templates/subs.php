<div class="content">

 <h3 class="sub-header" style="margin-left: 90px;">Dołącz do grona subskrypentów!</h3>
	
 <form class="form-horizontal" role="form" method="post" action="<?php echo $this -> siteUrl; ?>subs/add">
    
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Kategoria</label>
    <div class="col-sm-10" style="width: 300px;">
    	<select name="category">
		<option value="1">motoryzacja</option>
		<option value="2">komputery</option>
		<option value="3">gry</option>
	</select>
        <div>
  </div> 
        <br>
  <div class="form-group">
    <div class="col-sm-10" style="width: 300px;">
      <button type="submit" class="btn btn-default" name="submit">Wyślij</button>
    </div>
  </div>
  
 </form>
 
 <div class="alert alert-success" role="alert">
        <strong><?php echo $this -> response; ?></strong>
 </div>

</div>