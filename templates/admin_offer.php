<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

  <h2 class="sub-header">Propozycje produktów/usług</h2>
  <div class="table-responsive" style="max-width: 800px;">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Nazwa</th>
          <th>Użytkownik</th>
          <th>Akcja</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>Karcher SC 1.020 (1.512-211.0)</td>
          <td>testowy</td>
          <td>
                           <button type="button" class="btn btn-xs btn-primary">dodaj</button>
                           <button type="button" class="btn btn-xs btn-danger">usuń</button>
                          </td>
        </tr>
        <tr>
          <td>2</td>
          <td>Casio FX-991ES PLUS</td>
          <td>hortex92</td>
          <td>
                           <button type="button" class="btn btn-xs btn-primary">dodaj</button>
                           <button type="button" class="btn btn-xs btn-danger">usuń</button>
                          </td>
        </tr>
        <tr>
          <td>3</td>
          <td>Optoma Hd131Xe </td>
          <td>ewelina93</td>
          <td>
                           <button type="button" class="btn btn-xs btn-primary">dodaj</button>
                           <button type="button" class="btn btn-xs btn-danger">usuń</button>
                          </td>
        </tr>
        <tr>
          <td>4</td>
          <td>Motorola czytnik kodów kreskowych (LS2208)</td>
          <td>pikachu</td>
          <td>
                           <button type="button" class="btn btn-xs btn-primary">dodaj</button>
                           <button type="button" class="btn btn-xs btn-danger">usuń</button>
                          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>