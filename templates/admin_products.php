<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    
 <h2 class="sub-header">Produkty</h2>
  
  <div class="table-responsive" style="max-width: 800px;">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Nazwa</th>
          <th>Kategoria</th>
          <th>Flaga</th>
          <th>Akcja</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $n = count($this -> data);
        
        for($i=0;$i<$n;$i++) {
        ?>        
        <tr>
          <td><?php echo $this -> data[$i]['Id']; ?></td>
          <td><?php echo $this -> data[$i]['Name']; ?></td>
          <td><?php echo $this -> data[$i]['CategoryId']; ?></td>
          <td>
              <?php 
              if($this -> data[$i]['Flag'] == '')
                    echo 'oczekujący';
              else
                    echo 'zatwierdzony';
              ?>
          </td>
          <td>
            <a href="<?php echo $this -> siteUrl;?>admin/updateProduct/<?php echo $this -> data[$i]['Id']; ?>"><button type="button" class="btn btn-xs btn-primary">edytuj</button></a>
            <a href="<?php echo $this -> siteUrl;?>admin/deleteProduct/<?php echo $this -> data[$i]['Id']; ?>"><button type="button" class="btn btn-xs btn-danger">usuń</button></a>
          </td>
        </tr>
        <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>