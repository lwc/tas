<div class="content">

 <h3 class="sub-header" style="margin-left: 90px;">Zaproponuj produkt</h3>
	
 <form class="form-horizontal" role="form" method="post" action="<?php echo $this -> siteUrl; ?>offer/add">
  <div class="form-group">
    <label class="col-sm-2 control-label">Nazwa</label>
    <div class="col-sm-10" style="width: 300px;">
      <input name="name" type="text" class="form-control" placeholder="nazwa" required>
    </div>
  </div>
    
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Kategoria</label>
    <div class="col-sm-10" style="width: 300px;">
    	<select name="category">
		<option value="1">motoryzacja</option>
		<option value="2">komputery</option>
		<option value="3">gry</option>
	</select>
        <div>
  </div> 
    
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Opis</label>
    <div class="col-sm-10" style="width: 500px;">
      <textarea id="txt_content2" class="form-control" rows="3" name="description" required>opis...</textarea>
    </div>
  </div>
    
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default" name="submit">Wyślij</button>
    </div>
  </div>
  
 </form>

 <div class="alert alert-success" role="alert">
        <strong><?php echo $this -> response; ?></strong>
 </div>
 
</div>